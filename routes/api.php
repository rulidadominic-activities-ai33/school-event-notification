<?php

use App\Http\Controllers\EventController;
use App\Http\Controllers\MainController;
use App\Http\Controllers\StudentAuthController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\TeacherAuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function ($router) {

    Route::group(['prefix' => 'teacher'], function (){
        Route::post('login', [TeacherAuthController::class, 'login']);
        Route::post('register', [TeacherAuthController::class, 'register']);
        Route::post('logout', [TeacherAuthController::class, 'logout']);
        Route::post('me', [TeacherAuthController::class, 'me']);
    });
    
    Route::group(['prefix' => 'student'], function (){
        Route::post('login', [StudentAuthController::class, 'login']);
        Route::post('register', [StudentAuthController::class, 'register']);
        Route::post('logout', [StudentAuthController::class, 'logout']);
        Route::post('me', [StudentAuthController::class, 'me']);
    });
});


Route::apiResource('events', EventController::class);
Route::post('uploadFeaturedImage', [EventController::class, 'uploadFeaturedImage']);
Route::post('deleteFileFromServer/{filename}', [EventController::class, 'deleteFileFromServer']);

Route::get('student_event_list', [MainController::class, 'index']);
Route::get('single_event/{slug}', [MainController::class, 'singleEvent']);
