<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    use HasFactory;

    protected $fillable = [
        'teacher_account_id',
        'teacher_info_id',
    ];

    public function events() {
        return $this->hasMany(Event::class, 'teacher_id', 'id');
    }

    public function teacher_account() {
        return $this->hasOne(TeacherAccount::class,  'id');
    }

    public function teacher_info() {
        return $this->hasOne(TeacherInfo::class, 'id');
    }
}
