<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Event extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'description',
        'content',
        'featured_image',
        'slug',
        'teacher_id'
    ];

    public function teacher() {
        return $this->belongsTo(Teacher::class);
    }

    public function uniqueSlug($title){
        $slug = Str::slug($title, '-');
        $count = Event::where('slug', 'LIKE', "{$slug}%")->count();
        $newCount = $count > 0 ? ++$count : '';
        return $newCount > 0 ? "$slug-$newCount" : $slug;
    }

    public static function updateUniqueSlug($title){
        $slug = Str::slug($title, '-');
        $count = Event::where('slug', 'LIKE', "{$slug}%")->count();
        $newCount = $count > 1 ? ++$count : '';
        return $newCount > 1 ? "$slug-$newCount" : $slug;
    }
    
    public function setSlugAttribute($title){
        return $this->attributes['slug'] = $this->uniqueSlug($title);
     }
}
