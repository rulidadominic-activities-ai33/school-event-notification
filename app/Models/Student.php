<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;

    protected $fillable = [
        'student_account_id',
        'student_info_id',
    ];

    public function events() {
        return $this->hasMany(Event::class, 'teacher_id', 'id');
    }

    public function student_account() {
        return $this->hasOne(StudentAccount::class,  'id');
    }

    public function student_info() {
        return $this->hasOne(StudentInfo::class, 'id');
    }
}
