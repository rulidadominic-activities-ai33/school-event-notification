<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class StudentAccount extends Authenticatable implements JWTSubject
{
    use HasFactory;

    protected $fillable = [
        'student_number',
        'password',
        'status',
    ];

    protected $hidden = [
        'password',
    ];


    public function student() {
        return $this->belongsTo(Student::class);
    }

    public function getJWTIdentifier() {
        return $this->getKey();
    }
    public function getJWTCustomClaims() {
          return [];
    }
}
