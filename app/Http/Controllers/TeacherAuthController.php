<?php

namespace App\Http\Controllers;

use App\Models\Teacher;
use App\Models\TeacherAccount;
use App\Models\TeacherInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class TeacherAuthController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('auth:teacher', ['except' => ['login', 'register']]);
    }

    public function register(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
        ]);

        $teacher_account = [
            'username' => $request->username,
            'password' =>   Hash::make($request->password),
        ];

        $teacher_account = TeacherAccount::create($teacher_account);

        $teacher_info = [
            'first_name' => $request->first_name,
            'middle_name' => $request->middle_name,
            'last_name' => $request->last_name,
        ];

        $teacher_info = TeacherInfo::create($teacher_info);

        $teacher = [
            'teacher_account_id' => $teacher_account->id,
            'teacher_info_id' => $teacher_info->id,
        ];

        Teacher::create($teacher);


        return response()->json([ "message" => "Registered succesfully."]);
    }

    public function login(Request $request)
    {

        if (! $token = auth()->guard('teacher')->attempt(['username' => $request->username, 'password' => $request->password])) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        return $this->respondWithToken($token);
    }

    public function me()
    {
        return response()->json(Teacher::with(['teacher_info', 'teacher_account'])->where('id' ,Auth::id())->get());
    }

    public function logout()
    {

        auth('teacher')->logout();
        return response()->json(['message' => 'User logged out successfully!']);
    }

    public function refresh()
    {
        return $this->respondWithToken(auth('teacher')->refresh());
    }

    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('teacher')->factory()->getTTL() * 60,
            'user' => Teacher::with(['teacher_info', 'teacher_account'])->where('id' ,Auth::id())->get()
        ]);
    }
}
