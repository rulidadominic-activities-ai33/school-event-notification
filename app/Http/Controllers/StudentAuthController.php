<?php

namespace App\Http\Controllers;

use App\Models\Student;
use App\Models\StudentAccount;
use App\Models\StudentInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class StudentAuthController extends Controller
{

      public function __construct()
      {
          $this->middleware('auth:student', ['except' => ['login', 'register']]);
      }
  
      public function register(Request $request)
      {
          $this->validate($request, [
              'student_number' => 'required',
              'first_name' => 'required',
              'last_name' => 'required',
          ]);
  
          $student_account = [
              'student_number' => $request->student_number,
              'password' => Hash::make($request->student_number),
              'status' =>  'ACCOUNT-TEST',
          ];
  
          $student_account = StudentAccount::create($student_account);
  
          $student_info = [
              'first_name' => $request->first_name,
              'last_name' => $request->last_name,
          ];
  
          $student_info = StudentInfo::create($student_info);
  
          $student = [
              'student_account_id' => $student_account->id,
              'student_info_id' => $student_info->id,
          ];
  
          Student::create($student);
  
          return response()->json([ "message" => "Registered succesfully."]);
      }
  
      public function login(Request $request)
      {
  
          if (! $token = auth()->guard('student')->attempt(['student_number' => $request->student_number, 'password' => $request->student_number])) {
              return response()->json(['error' => 'Unauthorized'], 401);
          }
          return $this->respondWithToken($token);
      }
  
      public function me()
      { 
          return response()->json(Student::with(['student_info', 'student_account'])->where('id', Auth::id())->get());
      }
  
      public function logout()
      {
  
          auth('student')->logout();
          return response()->json(['message' => 'User logged out successfully!']);
      }
  
      public function refresh()
      {
          return $this->respondWithToken(auth('student')->refresh());
      }
  
      protected function respondWithToken($token)
      {
          return response()->json([
              'access_token' => $token,
              'token_type' => 'bearer',
              'expires_in' => auth('student')->factory()->getTTL() * 60,
              'user' => Student::with(['student_info', 'student_account'])->where('id', Auth::id())->get()
          ]);
      }
}
