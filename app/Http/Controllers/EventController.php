<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Models\Teacher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EventController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:teacher', ['except' => ['login', 'register']]);
    }

    //
    public function index() {
        return response()->json(Teacher::where('id', Auth::id())->with('events')->get());
    }

    public function store(Request $request) {


        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'content' => 'required',
            'featured_image' => 'required',
        ]);

        $data = [
            'slug' => $request->title,
            'title' => $request->title,
            'content' => $request->content,
            'description' => $request->description,
            'featured_image' => $request->featured_image,
            'teacher_id' => Auth::id()
        ];

        Event::create($data);

        return response()->json(['message' => "Event saved"]);
    }

    public function update(Request $request, $id) {

        $this->validate($request, [
            'title' => 'required',
            'content' => 'required',
            'featured_image' => 'required',
        ]);

        $data = [
            'title' => $request->title,
            'description' => $request->description,
            'content' => $request->content,
            'featured_image' => $request->featured_image,
            'slug' => Event::updateUniqueSlug($request->title),
            'teacher_id' => Auth::id()
        ];

        Event::where('id', $id)->update($data);
    }

    public function destroy($id) {
        $event = Event::where('id', $id)->first();
        $this->deleteFileFromServer($event->featured_image);
        Event::destroy($id);

    
    }

    public function deleteFileFromServer($filename){
        $filePath = public_path().'/uploads/'.$filename;
        if(file_exists($filePath)){
            @unlink($filePath);
        }
        return;
    }

    public function uploadFeaturedImage(Request $request){
        $picName = time().'.'.$request->file->extension();
        $request->file->move(public_path('uploads'), $picName);
        return $picName;
    }
}
