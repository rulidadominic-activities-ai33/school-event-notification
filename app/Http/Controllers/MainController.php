<?php

namespace App\Http\Controllers;

use App\Models\Event;
use Illuminate\Http\Request;

class MainController extends Controller
{
    //

    public function index() {
        return response()->json(Event::with('teacher.teacher_info')->get());
    }

    public function singleEvent($slug) {
       
        $event = Event::with(['teacher.teacher_info'])->where('slug', $slug)->first();
        return response()->json($event, 200);
    }
}
