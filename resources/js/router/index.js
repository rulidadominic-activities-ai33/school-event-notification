import Vue from 'vue';
import VueRouter from 'vue-router';

import Main from '../pages/Main.vue'
import Login from '../pages/Auth/Login.vue'
import Register from '../pages/Auth/Register.vue'
import Teacher from '../pages/Teacher/Teacher.vue'
import TeacherEvent from '../pages/Teacher/Event.vue'
import Student from '../pages/Student/Student.vue'
import StudentEvent from '../pages/Student/Event.vue'
import Event from '../pages/Event.vue'
import NotFound from '../pages/NotFound.vue'

const routes = [
    {
        path: '*',
        component: NotFound
    },
    {
        path: '/',
        component: Main
    },
    {
        path: '/login',
        component: Login
    },
    {
        path: '/register',
        component: Register
    },
    {
        path: '/:slug',
        name: "Event",
        component: Event
    },
    {
        path: '/teacher',
        component: Teacher,
        children: [
            { path: 'event', name: 'Teacher Event', component: TeacherEvent },
            { path: '', redirect: 'event' },
        ]
    },
    {
        path: '/student',
        component: Student,
        children: [
            { path: 'event', name: 'Student Event', component: StudentEvent },
            { path: '', redirect: 'event' },
        ]
    }



]

Vue.use(VueRouter);
export default new VueRouter({
    history: true,
    mode: "history",
    routes
});
