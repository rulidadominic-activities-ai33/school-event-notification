import axios from '../../axios';

export default {
    namespaced: true,

    state: {
        events: {},

    },
    getters: {
        GET_EVENTS: (state) => state.events,

    },
    mutations: {
        SET_EVENTS: (state, events) => state.events = events,

    },
    actions: {

        EVENTS: async ({ commit }) => {
            await axios.get('events')
                .then((response) => {
                    commit("SET_EVENTS", response.data[0].events);
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });
        },

        STORE: async ({ commit }, data) => {
            const response = await axios.post('events', data)
                .then((response) => {
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return response
        },

        DESTROY: async ({ commit }, id) => {
            const response = await axios.delete(`events/${id}`)
                .then((response) => {
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return response
        },

        UPDATE: async ({ commit }, { id, event }) => {
            const response = await axios.put(`events/${id}`, event)
                .then((response) => {
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return response
        },

        DELETE_IMAGE: async ({ commit }, fileName) => {
            const response = await axios.post(`deleteFileFromServer/${fileName}`)
                .then((response) => {
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return response
        },
    }
}