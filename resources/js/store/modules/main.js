import axios from "../../axios";
export default {
    namespaced: true,
    state: {
        event: {},
        events: {}
    },
    mutations: {
        SET_EVENT(state, data) {
            state.event = data;
        },
        SET_EVENTS(state, data) {
            state.events = data;
        },
    },
    getters: {
        GET_EVENT(state) {
            return state.event;
        },
        GET_EVENTS(state) {
            return state.events
        },
    },
    actions: {
        async EVENT({ commit }, slug) {
            const res = await axios.get(`single_event/${slug}`)
                .then((response) => {
                    commit("SET_EVENT", response.data);
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res
        },

        async EVENTS({ commit }) {
            await axios.get("student_event_list")
                .then((response) => {
                    commit("SET_EVENTS", response.data);
                })
                .catch((error) => {
                    return error.response;
                });
        },
    }
}