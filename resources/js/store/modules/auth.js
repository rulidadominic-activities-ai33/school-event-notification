import axios from '../../axios';

export default {
    namespaced: true,
    state: {
        student_token: localStorage.getItem('access_token') || '',
        student: {},
        teacher_token: localStorage.getItem('access_token') || '',
        teacher: {}
    },
    getters: {
        GET_TEACHER_TOKEN: (state) => state.teacher_token,
        GET_TEACHER: (state) => state.teacher,
        GET_STUDENT_TOKEN: (state) => state.student_token,
        GET_STUDENT: (state) => state.student,
        // GET_TOTAL_INFORMATION: (state) => state.information_count,
        // GET_TOTAL_USER: (state) => state.user_count,
        // GET_USER_ACCOUNTS: (state) => state.user_accounts,
        // GET_INFORMATIONS: (state) => state.informations,

    },
    mutations: {
        SET_TEACHER: (state, user) => {
            state.teacher = user;
            const BEARER_TOKEN = localStorage.getItem("access_token") || "";
            axios.defaults.headers.common["Authorization"] = `Bearer ${BEARER_TOKEN}`;
        },
        SET_TEACHER_TOKEN: (state, token) => {
            localStorage.setItem("access_token", token);
            localStorage.setItem('isAdmin', 'true');
            state.teacher_token = token;

            const BEARER_TOKEN = localStorage.getItem("access_token") || "";
            axios.defaults.headers.common["Authorization"] = `Bearer ${BEARER_TOKEN}`;
        },
        UNSET_TEACHER: (state) => {
            localStorage.removeItem("access_token");
            state.teacher_token = "";
            axios.defaults.headers.common["Authorization"] = "";
        },

        SET_STUDENT: (state, user) => {
            state.user = user;
            const BEARER_TOKEN = localStorage.getItem("access_token") || "";
            axios.defaults.headers.common["Authorization"] = `Bearer ${BEARER_TOKEN}`;
        },

        SET_STUDENT_TOKEN: (state, token) => {
            localStorage.setItem("access_token", token);
            localStorage.setItem('isAdmin', 'true');
            state.student_token = token;

            const BEARER_TOKEN = localStorage.getItem("access_token") || "";
            axios.defaults.headers.common["Authorization"] = `Bearer ${BEARER_TOKEN}`;
        },
        UNSET_STUDENT: (state) => {
            localStorage.removeItem("access_token");
            state.student_token = "";
            axios.defaults.headers.common["Authorization"] = "";
        },
    },
    actions: {
        TEACHER_LOGOUT: async ({ commit }) => {
            const res = await axios.post(`auth/teacher/logout?`)
                .then((response) => {
                    commit("UNSET_TEACHER");
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
        TEACHER_LOGIN: async ({ commit }, user) => {
            const res = await axios.post(`auth/teacher/login`, user)
                .then((response) => {
                    commit("SET_TEACHER", response.data.user);
                    commit("SET_TEACHER_TOKEN", response.data.access_token);
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
        TEACHER_REGISTER: async ({ commit }, teacher) => {
            const res = await axios.post(`auth/teacher/register`, teacher)
                .then((response) => {
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
        TEACHER_CHECK: async ({ commit }) => {
            const res = await axios.post(
                `auth/teacher/me?token=` + localStorage.getItem("access_token")
            )
                .then((response) => {
                    commit("SET_TEACHER", response.data);
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
        STUDENT_LOGOUT: async ({ commit }) => {
            const res = await axios.post(`auth/student/logout?`)
                .then((response) => {
                    commit("UNSET_STUDENT");
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
        STUDENT_LOGIN: async ({ commit }, student) => {
            const res = await axios.post(`auth/student/login`, student)
                .then((response) => {
                    commit("SET_STUDENT", response.data.user);
                    commit("SET_STUDENT_TOKEN", response.data.access_token);
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
        STUDENT_REGISTER: async ({ commit }, student) => {
            const res = await axios.post(`auth/student/register`, student)
                .then((response) => {
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
        STUDENT_CHECK: async ({ commit }) => {
            const res = await axios.post(
                `auth/student/me?token=` + localStorage.getItem("access_token")
            )
                .then((response) => {
                    commit("SET_STUDENT", response.data);
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },

    }
}