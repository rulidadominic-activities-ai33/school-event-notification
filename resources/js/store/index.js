import Vue from 'vue';
import Vuex from 'vuex';
import auth from './modules/auth';
import teacher from './modules/teacher';
import main from './modules/main';



Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        auth,
        teacher,
        main
    }
});